import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
@Component({
    selector: 'new-section',
    templateUrl: './new-section.component.html',
    styleUrls: ['./new-section.component.css']
})
export class SectionDialogComponent implements OnInit {

    form: FormGroup;
    description:string;
    name:string;
    title:string;
    constructor(private MessageService: MessageService,private sectionService: SectionService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<SectionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.name = data.name;
        this.title = data.title;
    }

    ngOnInit() {
        this.form = this.fb.group({
            description: [this.description, []],
            name: [this.name, []]
        });
    }

    save() {
        const formValue = this.form.value;
      const newSection = new Section(
          null,
        formValue['name'],
        formValue['description'],
        null,
        null, 
        false
      );
        this.sectionService.save(newSection).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("section added now")
        })
        
        this.dialogRef.close(this.form.value);
    }

    close() {
        this.dialogRef.close();
    }
}