import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
@Component({
    selector: 'delete-section',
    templateUrl: './delete-section.component.html',
    styleUrls: ['./delete-section.component.scss']
})
export class SectionDeleteComponent implements OnInit {
    id: number;
    title:string;
   

    constructor(private MessageService: MessageService,private sectionService: SectionService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<SectionDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.id = data.id;
            this.title = data.title;
    }

    ngOnInit() {
    }

    delete() {
        this.sectionService.delete(this.id).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("section deleted")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}