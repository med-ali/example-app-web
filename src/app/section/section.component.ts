import { Component, OnInit } from '@angular/core';
import { SectionService } from '../_services/section.service';
import { first } from 'rxjs/operators';
import { User  } from '../_models/user';
import { ITEMS_PER_PAGE } from '../shared/pagination.constant';
import { SectionDialogComponent } from './new-section.component';
import { MatTableDataSource, MatDialog, MatDialogConfig, PageEvent } from '@angular/material';
import { Subscription } from 'rxjs';
import { MessageService  } from '../_services/message.service';
import { SectionDeleteComponent } from './delete-section.component';
import { Section } from '../_models';

@Component({
  selector: 'app-section-component',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss']
})
export class SectionComponent implements OnInit {
  users: User[];
  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  pageEvent: PageEvent;
  pageIndex:number;
  pageSize:number;
  length:number;
  public displayedColumns = ['name', 'description',  'subject', 'delete'
];
  public dataSource = new MatTableDataSource<User>();
  subscription: Subscription;
  message: any;
  sections: Section[];
  isAdmin: boolean;
  constructor(private MessageService: MessageService,private sectionService: SectionService,private dialog: MatDialog) { 
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "section added now" || message["text"] == "section edited now"|| message["text"] == "section deleted" )  
      {
        this.getServerData(this.pageEvent);
        this.message = message;
      }
    }); 
    if (localStorage.getItem('isAdmin') == "true") {
      // logged in so return true
      this.isAdmin = true;
    }  
  }

  ngOnInit() {
    this.sections = [];
    this.currentPage = 0;
    this.size = ITEMS_PER_PAGE;
    this.sortBy = "id"
    this.desc = -1
    this.pageEvent = new PageEvent();
    this.pageEvent.pageSize = ITEMS_PER_PAGE;
    this.pageEvent.pageIndex = 0;
    
    //this.loadAllSections();
    this.getServerData(this.pageEvent)
  }
  private loadAllSections() {
    this.sectionService.getAll({page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
        this.sections = data["content"];
        this.pageEvent.length = data["totalElements"];
        this.pageEvent.pageSize = ITEMS_PER_PAGE;
        this.pageEvent.pageIndex = 0;
    });
}
applyFilter(q:string){
  console.log('str'+q)
  this.sectionService.getAllWithSearch({page:this.pageIndex,size:this.size,sortBy:this.sortBy,desc:this.desc,q:q}).pipe(first()).subscribe(data => { 
    this.dataSource.data = data["content"];
    this.sections = data["content"];
});
}
redirectToDelete(id:number){
  const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        id: id,
        title: 'Supprimer section'
    };

    this.dialog.open(SectionDeleteComponent, dialogConfig);
}

openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        id: 1,
        title: 'Nouveau section'
    };

    this.dialog.open(SectionDialogComponent, dialogConfig);
}
public getServerData(event?:PageEvent){//0:0:10:0
  console.log('event'+event.length+":"+event.pageIndex+":"+event.pageSize+":"+event.previousPageIndex)
  this.sectionService.getAll({page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response =>{
      console.log('response'+response)
        this.sections = response["content"];
        this.dataSource.data = response["content"];
        //this.datasource = response.data;
        this.pageIndex = event.pageIndex;
        this.pageSize = event.pageSize;
        this.length = response["totalElements"];
      
    },
    error =>{
      // handle error
    }
  );
  return event;
}
}
