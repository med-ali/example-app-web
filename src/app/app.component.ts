import { Component } from '@angular/core';
import { MessageService  } from './_services/message.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'example-app';
  subscription: Subscription; 
  isLogin: boolean;
  login: string;
  constructor(private MessageService: MessageService) {
    this.login =localStorage.getItem('isLogin');
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "login" )  
      {
        this.isLogin = true;
      }
      else if (message["text"] == "logout" ){
        this.isLogin = false;
      }
      if (this.login  == "true" )  
      {
        this.isLogin = true;
      }
      else if (this.login== "false" ){
        this.isLogin = false;
      }
    });
    if (this.login  == "true" )  
      {
        this.isLogin = true;
      }
      else if (this.login== "false" ){
        this.isLogin = false;
      }
   }
}
