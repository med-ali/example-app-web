import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { AuthGuard } from './_guards';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { UserComponentComponent } from './user-component/user-component.component';
import { SectionComponent } from './section/section.component';
import { SectionDialogComponent } from './section/new-section.component';
import { SubjectComponent } from './subject/subject.component';
import { SubjectSectionComponent } from './subject/subjectSection.component';
import { DocumentComponent } from './document/document.component';
import { MeetingComponent } from './meeting/meeting.component';
import { MyMeetingComponent } from './meeting/MyMeeting.component';
import { MyConnectMeetingComponent } from './meeting/MyConnectMeeting.component';
import { UserMetingComponentComponent } from './meeting/user-meeting.component';
import { ShowMetingComponentComponent } from './meeting/show-meeting.component';
const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
const appRoutes: Routes = [
    { path: '', component: SectionComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent},
    { path: 'changepassword', component: ChangepasswordComponent },
    { path: 'users', component: UserComponentComponent , canActivate: [AuthGuard]},
    // otherwise redirect to home
    { path: 'sections', component: SectionComponent, canActivate: [AuthGuard] },
    { path: 'subjects', component: SubjectComponent, canActivate: [AuthGuard] },
    {path: 'section/:id/subject', component: SubjectSectionComponent, canActivate: [AuthGuard]},
    {path: 'subject/:id/documents', component: DocumentComponent, canActivate: [AuthGuard]},
    {path: 'mymeeting/:id/admin', component: MyMeetingComponent, canActivate: [AuthGuard]},
    { path: 'meeting', component: MeetingComponent, canActivate: [AuthGuard] },
    {path: 'mymeeting/:id/connect', component: MyConnectMeetingComponent, canActivate: [AuthGuard]},
    {path: 'meeting/:id/user', component: UserMetingComponentComponent, canActivate: [AuthGuard]},
    {path: 'meeting/:id', component: ShowMetingComponentComponent, canActivate: [AuthGuard]},
        //{path: 'subject/:id/prof',
        //component: SubjectSectionComponent,canActivate: [AuthGuardService]},
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);