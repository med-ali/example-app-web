import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { User } from '../_models';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../_services';
import { UserService } from '../_services';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService  } from '../_services/message.service';

@Component({
  selector: 'app-my-nav',
  templateUrl: './my-nav.component.html',
  styleUrls: ['./my-nav.component.scss']
})
export class MyNavComponent {
  currentUserSubscription: Subscription;
  currentUser: User;
  checkLoginSubscription: Subscription;
  isLogin:Boolean;
  currentAdminSubscription: Subscription;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  isAdmin: boolean;

  constructor(private MessageService: MessageService,
    private breakpointObserver: BreakpointObserver,private userService: UserService
    ,private authenticationService: AuthenticationService,private router: Router) {
    
      this.isAdmin = false; 
    if (this.authenticationService.currentUser)
    {
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
             this.currentUser = user;
        });
   }
   if (this.authenticationService.isLogin)
   {
       this.currentUserSubscription = this.authenticationService.isLogin.subscribe(isLogin => {
            this.isLogin = isLogin;
       });
       /*this.currentAdminSubscription = this.authenticationService.isAdmin.subscribe(isAdmin => {
        this.isAdmin = isAdmin;
   });*/
        
   }
   if (localStorage.getItem('isLogin') == "true") {
    // logged in so return true
    this.isLogin = true;
  }
  if (localStorage.getItem('isAdmin') == "true") {
    // logged in so return true
    this.isAdmin = true;
    console.log('dedededed'+this.isAdmin)

  }
  }
  test(){
    this.userService.getAll().subscribe(
      date =>{
        console.log('hello ')
      }
    )
  }
  getBac(type:string){
    this.userService.getAll().subscribe(
      date =>{
        console.log('hello ')
      }
    )
  }
  testp(){
    this.userService.getAllp().subscribe(
      date =>{
        console.log('hello ')
      }
    )
  }
  logout() {
    this.MessageService.sendMessage("logout")
    console.log("logout")
    this.router.navigate(["/login"]);
    this.authenticationService.logout()
  }
}
