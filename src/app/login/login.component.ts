import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MessageService  } from '../_services/message.service';
import { AlertService, AuthenticationService } from '../_services';

@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    returnUrlUser: any;
    returnUrlAdmin: any;

    constructor(
        private MessageService: MessageService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
        this.authenticationService.logout();
        this.returnUrlUser = 'sections';
        // get return url from route parameters or default to '/'
        this.returnUrlAdmin = this.route.snapshot.queryParams['returnUrl'] || 'users';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        //this.loading = true;
        this.authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.MessageService.sendMessage("login")
                    if (data.authorities[0].authority == "ROLE_ADMIN"){
                        this.router.navigate([this.returnUrlAdmin]);
                    }
                    else{
                        this.router.navigate([this.returnUrlUser]);
                    }
                },
                error => {
                    this.alertService.error(error);
                });
    }
}
