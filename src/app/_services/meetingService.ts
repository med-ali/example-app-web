import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Section } from '../_models';
import { Observable } from 'rxjs';
import { SubjectSection } from '../_models/subjectSection';
import { Meeting } from '../_models/meeting';

@Injectable()
export class MeetingService {
    constructor(private http: HttpClient) { }
    getMeetingByUser(id:number,req?:any) {
        return this.http.get<Meeting[]>(`${environment.apiUrl}/api/meetings/user/`+id, { params: req });
    }
    getAllMeeting(req?:any) {
        return this.http.get<Meeting[]>(`${environment.apiUrl}/api/meetings`, { params: req });
    }
    getAllWithSearch(req?:any){
        return this.http.get<Meeting[]>(`${environment.apiUrl}/api/_search/meetings`, { params: req });
    }
    getById(id: number) {
        return this.http.get<Meeting>(`${environment.apiUrl}/api/meetings/` + id);
    }

    save(section: Meeting,id:number) {
        return this.http.post(`${environment.apiUrl}/api/meetings/`+id, section);
    }
    connectToMeeting(meeting:Meeting,userId:number) {
        return this.http.post(`${environment.apiUrl}/api/meetings/` +meeting.id +"/user/"+userId   ,meeting);
    }
    getConnectByMeeting(meetingId: number,req?:any) {
        return this.http.get(`${environment.apiUrl}/api/meetings/connect/` + meetingId , { params: req });
    }
    getMeetingByUserAdmin(userId: number,req?:any) {
        return this.http.get(`${environment.apiUrl}/api/meetings/admin/` + userId , { params: req });
    }
    getMeetingNotConnectByUser(userId: number,req?:any) {
        return this.http.get(`${environment.apiUrl}/api/meetings/notconnect/` + userId , { params: req });
    }
    getMeetingNotConnectByUserSearch(userId: number,req?:any) {
        return this.http.get(`${environment.apiUrl}/api/meetings/notconnect/` + userId , { params: req });
    }
    getUsertConnectByMeeting(meetingId: number,req?:any) {
        return this.http.get(`${environment.apiUrl}/api/meetings/user/connect/` + meetingId , { params: req });
    }
    getMessageByMeeting(meetingId: number,req?:any) {
        return this.http.get(`${environment.apiUrl}/api/meetings/message/` + meetingId , { params: req });
    }
    update(user: Meeting) {
        return this.http.put(`${environment.apiUrl}/sections/`, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/meetings/` + id);
    }

    pushFileToStorageg(file: File): Observable<any> {
        const data: FormData = new FormData();
        data.append('file', file);
        return this.http.post(environment.apiUrl + '/api/savefile',data,{
            reportProgress: true,
            responseType: 'text'
            })
    }
    pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
        const headers = new HttpHeaders();
            /** No need to include Content-Type in Angular 4 */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            const options = { headers: headers};
        const data: FormData = new FormData();
        data.append('file', file);
        const newRequest = new HttpRequest('POST', environment.apiUrl + '/api/savefile', data,options );
        return this.http.request(newRequest);
        }
}