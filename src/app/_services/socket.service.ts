import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { Messsage } from '../_models/message';

@Injectable()
export class SocketService {
  url: string = "http://172.17.0.2:8081/" + "api/socket";

  constructor(private http: HttpClient) { }

  post(data: Messsage) {
    return this.http.post<Messsage>(this.url, data);
  }
}
