import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../_models';
import { Router, ActivatedRoute } from '@angular/router';
@Injectable()
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    private checkLoginSubject: BehaviorSubject<Boolean>;
    public isLogin: Observable<Boolean>;
    public currentUser: Observable<User>;
    public isAdmin: Observable<Boolean>;
    private checkAdminSubject: BehaviorSubject<Boolean>;
    constructor(private http: HttpClient,private router: Router,) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        this.checkLoginSubject = new BehaviorSubject<Boolean>(false);
        this.checkAdminSubject = new BehaviorSubject<Boolean>(false);
        this.isLogin = this.checkLoginSubject.asObservable();
        this.isAdmin = this.checkAdminSubject.asObservable();
    }
    
    login(username: string, password: string) {
        console.log('done')
        return this.http.post<any>(`${environment.apiUrl}/api/authenticate`, { email: username, password: password })
            .pipe(map(data => {
                // login successful if there's a jwt token in the response
                console.log('eror'+data)
                if (data && data.jwtToken) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem("jwtToken", data.jwtToken);
                    localStorage.setItem('currentUser', JSON.stringify(data.user));
                    localStorage.setItem('userId', data.user.id);
                    localStorage.setItem('isLogin',"true");
                    localStorage.setItem("role", data.user.authorities[0].authority);
                    localStorage.setItem("isAdmin", "false");
                    if (data.user.authorities[0].authority == "ROLE_ADMIN"){
                        localStorage.setItem("isAdmin", "true");
                        this.checkAdminSubject.next(true);
                    }else{
                        console.log('data.user.authorities[0].authority'+data.user.authorities[0].authority)
                        //localStorage.setItem("isAdmin", "false");
                        this.checkAdminSubject.next(false);
                    }
                    console.log('localStorage.setItem("role", data.user.authorities[0].authority);'+localStorage.getItem("role"))
                    this.currentUserSubject.next(data.user);
                }
                
                localStorage.setItem("isLogin", "true");
                this.checkLoginSubject.next(true);
                
                return data.user;
            },
         error =>  {
             console.log('error http')
         } ,
            ));
    }
    changepassword(username: string, password: string) {
        console.log('done')
        return this.http.put<any>(`${environment.apiUrl}/api/updatepassword`, { email: username, password: password })
            .pipe(map(data => {
                // login successful if there's a jwt token in the response
                console.log('eror'+data)
                return data;
            },
         error =>  {
             console.log('error http'+error)
         } ,
            ));
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('isLogin');
        this.currentUserSubject.next(null);
        this.checkLoginSubject.next(false);
    }
    createUser(user: any){
        return this.http.post(environment.apiUrl + '/api/signup', user);
      }
}