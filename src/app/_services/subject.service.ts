import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Section } from '../_models';
import { Subject } from '../_models';
import { Observable } from 'rxjs';

@Injectable()
export class SubjectService {
    constructor(private http: HttpClient) { }

    getAll(req?:any) {
        return this.http.get<Subject[]>(`${environment.apiUrl}/api/subjects`, { params: req });
    }
    getAllWithSearch(req?:any){
        return this.http.get<Subject[]>(`${environment.apiUrl}/api/_search/subjects`, { params: req });
    }
    getById(id: number) {
        return this.http.get<Subject>(`${environment.apiUrl}/api/subjects/` + id);
    }
    getSubjectsBySectionWithSearch(sectionId:number,req?:any){
        return this.http.get<Subject[]>(`${environment.apiUrl}/api/_search/subjects/`+sectionId+"/sections", { params: req });
    }
    save(section: Section) {
        return this.http.post(`${environment.apiUrl}/api/subjects`, section);
    }

    update(user: Section) {
        return this.http.put(`${environment.apiUrl}/subjects/`, user);
    }
    deleteSubjectSection(subjectId:number,sectionId:number){
        return this.http.delete(`${environment.apiUrl}/api/subjects/` + subjectId + "/sections/"+sectionId);
    }
    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/subjects/` + id);
    }

    getSubjectsBySection(id:number,req?:any) {
        return this.http.get<Subject[]>(`${environment.apiUrl}/api/subjects/`+id+"/sections", { params: req });
    }
    getSubjectsBySectionNotAssociated(id:number,req?:any) {
        return this.http.get<Subject[]>(`${environment.apiUrl}/api/subjects/`+id+"/not/sections", { params: req });
    }
    pushFileToStorageg(file: File): Observable<any> {
        const data: FormData = new FormData();
        data.append('file', file);
        return this.http.post(environment.apiUrl + '/api/savefile',data,{
            reportProgress: true,
            responseType: 'text'
            })
    }
    pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
        const headers = new HttpHeaders();
            /** No need to include Content-Type in Angular 4 */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            const options = { headers: headers};
        const data: FormData = new FormData();
        data.append('file', file);
        const newRequest = new HttpRequest('POST', environment.apiUrl + '/api/savefile', data,options );
        return this.http.request(newRequest);
        }
}