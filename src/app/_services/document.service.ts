import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest, HttpResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Section } from '../_models';
import { Subject,Document } from '../_models';
import { Observable } from 'rxjs';

@Injectable()
export class DocumentService {
    constructor(private http: HttpClient) { }

    getAll(req?:any) {
        return this.http.get<Document[]>(`${environment.apiUrl}/api/documents`, { params: req });
    }
    getAllWithSearch(req?:any){
        return this.http.get<Document[]>(`${environment.apiUrl}/api/_search/documents`, { params: req });
    }
    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/api/documents/` + id);
    }
    getDocumentsBySubjectWithSearch(subjectId:number,req?:any){
        return this.http.get<Document[]>(`${environment.apiUrl}/api/_search/documents/`+subjectId+"/subjects", { params: req });
    }
    save(d: Document) {
        return this.http.post(`${environment.apiUrl}/api/documents`, d);
    }

    deleteDocumentBySubject(documentId:number,subjectId:number){
        return this.http.delete(`${environment.apiUrl}/api/documents/` + documentId + "/subjects/"+subjectId);
    }
    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/documents/` + id);
    }

    getDocumentsBySubject(id:number,req?:any) {
        return this.http.get<Document[]>(`${environment.apiUrl}/api/documents/`+id+"/subjects", { params: req });
    }
    addlikeToDocument(idUser:number,item:Document) {
        return this.http.put<any>(`${environment.apiUrl}/api/documents/`+"vote/"+idUser, item);
    }
    /*downloadDocument(id:number) {
        return this.http.get<any>(`${environment.apiUrl}/api/documents/`+id);
    }
    printReport(type:string): Observable<any> {
        return this.http.get<any>(this.resourceUrl+"/print/?type="+type);
    }*/
    downloadDocument(id:number) : Observable<HttpResponse<any>>{
        return this.http.get<any>(`${environment.apiUrl}/api/documents/download/`+id, {  observe: 'response' });
    }
    downloadDocuments(id:number) :Observable<HttpResponse<any>> {
        console.log( this.http.get<any>(`${environment.apiUrl}/api/documents/download/`+id ))
        return this.http.get<any>(`${environment.apiUrl}/api/documents/download/`+id,{ observe: 'response' } );
    }
    pushFileToStorageg(file: File): Observable<any> {
        const data: FormData = new FormData();
        data.append('file', file);
        return this.http.post(environment.apiUrl + '/api/savefile',data,{
            reportProgress: true,
            responseType: 'text'
            })
    }
    pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
        const headers = new HttpHeaders();
            /** No need to include Content-Type in Angular 4 */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            const options = { headers: headers};
        const data: FormData = new FormData();
        data.append('file', file);
        const newRequest = new HttpRequest('POST', environment.apiUrl + '/api/savefile', data,options );
        return this.http.request(newRequest);
        }
}