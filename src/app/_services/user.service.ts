import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpEvent, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../_models';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
  
    constructor(private http: HttpClient) { }
    getAllWithSearch(req?:any) {
        return this.http.get<User[]>(`${environment.apiUrl}/api/search/users`, { params: req });
      }
    getAll(req?:any) {
        return this.http.get<User[]>(`${environment.apiUrl}/api/users`, { params: req });
    }
    getAllp() {
        let headers = new HttpHeaders();
        headers.append('Authorization',localStorage.getItem('jwtToken'))
        console.log("getall")
          return this.http.get<any[]>(`${environment.apiUrl}/api/testproject`);
      }
    getById(id: number) {
        return this.http.get(`${environment.apiUrl}/users/` + id);
    }

    register(user: User) {
        console.log('faaaaaaaaaaaaaaaaaaa'+user.password)
        return this.http.post(`${environment.apiUrl}/api/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`${environment.apiUrl}/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/api/users/` + id);
    }

    pushFileToStorageg(file: File): Observable<any> {
        const data: FormData = new FormData();
        data.append('file', file);
        return this.http.post(environment.apiUrl + '/api/savefile',data,{
            reportProgress: true,
            responseType: 'text'
            })
    }
    pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
        const headers = new HttpHeaders();
            /** No need to include Content-Type in Angular 4 */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            const options = { headers: headers};
        const data: FormData = new FormData();
        data.append('file', file);
        const newRequest = new HttpRequest('POST', environment.apiUrl + '/api/savefile', data,options );
        return this.http.request(newRequest);
        }
}