import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { MeetingService } from '../_services/meetingService';
import { Meeting } from '../_models/meeting';
import { User } from '../_models';
@Component({
    selector: 'connect-meeting',
    templateUrl: './connect-meeting.component.html', 
})
export class MeetingConnectComponent implements OnInit { 
    title:string;
    meeting:Meeting;
    users:User;
    constructor(private MessageService: MessageService,private meetingService: MeetingService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<MeetingConnectComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.users = data.users;
            this.title = data.title;
    }

    ngOnInit() {
    }

    getConnectByMeeting() {
        this.meetingService.getConnectByMeeting(this.meeting.id).subscribe(data => {
           //this.MessageService.sendMessage("meeting deleted")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}