import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService  } from '../_services/message.service';
import { SubjectService, DocumentService, AuthenticationService } from '../_services';
import { MatDialog, MatDialogConfig, MatIconRegistry, PageEvent } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ITEMS_PER_PAGE } from '../shared';
import { Subject,Document, User } from '../_models'; 
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser'; 
import { MeetingService } from '../_services/meetingService';
import { Meeting } from '../_models/meeting';
import { DocumentDialogComponent } from '../document/new-document.component';
import { MeetingDialogComponent } from './new-meeting.component';

@Component({
  selector: 'app-connect-meeting',
  templateUrl: './MyConnectMeeting.component.html'
})
export class MyConnectMeetingComponent implements OnInit {
  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  subscription: Subscription;
  message: any;
  section: any;
  documents:Document[];
  subject:Subject;
  docType: string;
  headers: string[];
  currentUserSubscription: Subscription;
  currentUser: User;
  pageEvent: PageEvent;
  pageIndex:number;
  pageSize:number;
  length:number;
  meetings: Meeting[];
  isAdmin: boolean;
  userId: any;
 
  constructor(private meetingService: MeetingService ,private domSanitizer: DomSanitizer, private matIconRegistry: MatIconRegistry,private http: HttpClient,
    private authenticationService: AuthenticationService,
    private MessageService: MessageService,private subjectService: SubjectService,private documentService: DocumentService,
    private dialog: MatDialog,private router: Router,private route: ActivatedRoute) {
      this.currentPage = 0;
      this.size = ITEMS_PER_PAGE;
      this.sortBy = "id"
      this.desc = -1
      this.pageEvent = new PageEvent();
      this.pageEvent.pageSize = ITEMS_PER_PAGE;
      this.pageEvent.pageIndex = 0;
      //this.loadAllDocuments(
        this.userId =localStorage.getItem('userId');
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
          this.currentUser = user;
          this.getServerDataAllMeetingByUserConnect(this.pageEvent)
        });
        if (localStorage.getItem('isAdmin') == "true") {
          // logged in so return true
          this.isAdmin = true;
        }
  }
   
  public getServerDataAllMeetingByUserConnect(event?:PageEvent){//0:0:10:0
    this.meetingService.getConnectByMeeting(this.userId,{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
      response =>{
        console.log('response'+response)
          this.meetings = response["content"]; 
          //this.datasource = response.data;
          this.pageIndex = event.pageIndex;
          this.pageSize = event.pageSize;
          this.length = response["totalElements"];
        
      },
      error =>{
        // handle error
      }
    );
    return event;
  }
  delete(item:Document){
    this.documentService.delete(item.id).pipe(first()).subscribe(data => {
    });
  }
  getUsertConnectByMeeting(item:Meeting){
    this.meetingService.getUsertConnectByMeeting(item.id,{page:0,size:5,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => {
    });
  }
 /* redirectToDelete(id:number){
    const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      dialogConfig.data = {
          id: id,
          title: 'Supprimer document'
      };
  
      this.dialog.open(DocumentDeleteComponent, dialogConfig);
  }*/

  ngOnInit() {
    this.meetings = [];
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        idUser: this.currentUser.id,
        title: 'Nouveau meeting'
    };

    this.dialog.open(MeetingDialogComponent, dialogConfig);
}
applyFilter(q:string){
    console.log('str'+q)
    this.meetingService.getConnectByMeeting(this.currentUser.id,{q:q,page:this.pageIndex,size:this.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
      response => {
        this.meetings = response["content"]; 
        //this.datasource = response.data;
        this.length = response["totalElements"];  });
  }
}
