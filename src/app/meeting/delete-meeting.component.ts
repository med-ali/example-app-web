import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { MeetingService } from '../_services/meetingService';
@Component({
    selector: 'delete-meeting',
    templateUrl: './delete-meeting.component.html', 
})
export class MeetingDeleteComponent implements OnInit {
    id: number;
    title:string;
   
    constructor(private MessageService: MessageService,private meetingService: MeetingService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<MeetingDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.id = data.id;
            this.title = data.title;
    }

    ngOnInit() {
    }

    delete() {
        this.meetingService.delete(this.id).subscribe(data => {
           this.MessageService.sendMessage("meeting deleted")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}