import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { MeetingService } from '../_services/meetingService';
import { Meeting } from '../_models/meeting';
@Component({
    selector: 'new-meeting',
    templateUrl: './new-meeting.component.html',
    styleUrls: ['./new-meeting.component.scss']
})
export class MeetingDialogComponent implements OnInit {

    form: FormGroup;
    description:string;
    name:string;
    title:string;
    idUser:number;
    constructor(private MessageService: MessageService,private meetingService: MeetingService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<MeetingDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.name = data.name;
        this.title = data.title;
        this.idUser = data.idUser
    }

    ngOnInit() {
        this.form = this.fb.group({
            description: [this.description, []],
            name: [this.name, []]
        });
    }

    save() {
        const formValue = this.form.value;
      const newMeeting = new Meeting(
        null,
        null,
        formValue['name'],
        formValue['description'],
        null, 
        false
      );
        this.meetingService.save(newMeeting,this.idUser).subscribe(data => {
           this.MessageService.sendMessage("meeting added now")
        })
        
        this.dialogRef.close(this.form.value);
    }

    close() {
        this.dialogRef.close();
    }
}