import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { User  } from '../_models/user';
import { ITEMS_PER_PAGE } from '../shared/pagination.constant';
import { MatTableDataSource, MatDialogConfig, MatDialog, PageEvent } from '@angular/material';
import { Subscription } from 'rxjs';
import { MessageService  } from '../_services/message.service';
import { Meeting } from '../_models/meeting';
import { ActivatedRoute } from '@angular/router';
import { MeetingService } from '../_services/meetingService';

@Component({
  selector: 'app-user-meeting-component',
  templateUrl: './user-meeting.component.html',
  styleUrls: ['./user-meeting.component.scss']
})
export class UserMetingComponentComponent implements OnInit {
  users: User[];
  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  public displayedColumns = ['nom', 'prenom', 'email', 'update', 'delete'];
  public dataSource = new MatTableDataSource<User>();
  subscription: Subscription; 
  message: any;
  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number;
  length:number;
  constructor(private MessageService: MessageService,private userService: UserService,
    private dialog: MatDialog,private route: ActivatedRoute,private meetingService: MeetingService) {
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "user added now" || message["text"] == "user edited now"|| message["text"] == "user deleted now" )  
      {
        //this.loadAllUsers()
        this.getServerData(this.pageEvent)
        this.message = message;
      }
    });
   }

  ngOnInit() {
    this.currentPage = 0;
    this.size = ITEMS_PER_PAGE;
    this.sortBy = "id"
    this.desc = -1
    //this.loadAllUsers();
    this.pageEvent = new PageEvent();
    this.pageEvent.pageSize = ITEMS_PER_PAGE;
    this.pageEvent.pageIndex = 0;
    this.getServerData(this.pageEvent)
  }
  public getServerData(event?:PageEvent){//0:0:10:0
    console.log('event'+event.length+":"+event.pageIndex+":"+event.pageSize+":"+event.previousPageIndex)
    this.meetingService.getUsertConnectByMeeting(this.route["snapshot"]["params"]["id"],{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
      response =>{
        console.log('response'+response)
          this.users = response["content"];
          this.dataSource.data = response["content"];
          //this.datasource = response.data;
          this.pageIndex = event.pageIndex;
          this.pageSize = event.pageSize;
          this.length = response["totalElements"];        
      },
      error =>{
        // handle error
      }
    );
    return event;
  }
  getUsertConnectByMeeting(item:Meeting){
    this.meetingService.getUsertConnectByMeeting(item.id,{page:0,size:5,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => {
    });
  }
  /*redirectToDelete(id:number){
    const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      dialogConfig.data = {
          id: id,
          title: 'Supprimer un utilisateur'
      };
  
      this.dialog.open(UserDeleteComponent, dialogConfig);
  }*/


applyFilter(q:string){
  console.log('str'+q)
  this.meetingService.getUsertConnectByMeeting(this.route["snapshot"]["params"]["id"],{q:q,page:this.pageIndex,size:this.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response =>{
      console.log('response'+response)
        this.users = response["content"];
        this.dataSource.data = response["content"];  
    },
    error =>{
      // handle error
    }
  );
}
}

