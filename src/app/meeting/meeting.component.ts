import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as Stomp from "@stomp/stompjs"
import * as SockJS from 'sockjs-client';
 import { SocketService } from '../_services/socket.service'; 
import { Messsage } from '../_models/message';
import { Subscription } from 'rxjs';
import { AuthenticationService } from '../_services';
import { User } from '../_models';
import { MatDialogConfig, MatDialog, PageEvent } from '@angular/material';
import { MeetingDialogComponent } from './new-meeting.component';
import { MeetingService } from '../_services/meetingService';
import { Meeting } from '../_models/meeting';
import { ITEMS_PER_PAGE } from '../shared';
import { MessageService  } from '../_services/message.service';
import { MeetingDeleteComponent } from './delete-meeting.component';
import { MeetingConnectComponent } from './connect-meeting.component';

 @Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html', 
})
export class MeetingComponent implements OnInit {
  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  pageEvent: PageEvent;
  pageIndex:number;
  pageSize:number;
  length:number;
  q:string;
  private serverUrl = "http://localhost:8081/" + 'socket'
  isLoaded: boolean = false; 
  showCode:boolean = false;
  isCustomSocketOpened = false;
  private stompClient;
  private form: FormGroup;
  private userForm: FormGroup;
  messages: Messsage[] = [];
  currentUserSubscription: Subscription;
   currentUser: User;
   isAdmin: boolean;
   meetings: Meeting[];
   subscription: Subscription;
   message: any;
   userId: any;
  constructor(private MessageService: MessageService,private meetingService: MeetingService ,private socketService: SocketService 
    ,private authenticationService: AuthenticationService,private dialog: MatDialog) {  
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
    });
    this.userId =localStorage.getItem('userId');
    if (localStorage.getItem('isAdmin') == "true") {
      // logged in so return true
      this.isAdmin = true;
    }
    this.currentPage = 0;
    this.size = ITEMS_PER_PAGE;
    this.sortBy = "id"
    this.desc = -1
    this.pageEvent = new PageEvent();
    this.pageEvent.pageSize = ITEMS_PER_PAGE;
    this.pageEvent.pageIndex = 0;  
    //this.loadAllSections();
    this.getServerDataAllMeeting(this.pageEvent)
   }

  ngOnInit() {
    this.meetings = [];
    this.form = new FormGroup({
      message: new FormControl(null, [Validators.required])
    })
    this.userForm = new FormGroup({
      fromId: new FormControl(null, [Validators.required]),
      toId: new FormControl(null)
    })
    this.initializeWebSocketConnection();
    this.openSocket();
    
  } 
  /*sendMessageUsingSocket() {
    if (this.form.valid) {
      let message: Message = { message: this.form.value.message, fromId: this.userForm.value.fromId, toId: this.userForm.value.toId };
      this.stompClient.send("/socket-subscriber/send/message", {}, JSON.stringify(message));
    }
  }*/

  /*sendMessageUsingRest() {
    if (this.form.valid) {
      let message: Message = { message: this.form.value.message, fromUserId: "eded" ,fromId: this.userForm.value.fromId, toId: this.userForm.value.toId };
      this.socketService.post(message).subscribe(res => {
        console.log(res);
      })
    }
  }*/

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.isLoaded = true;
      that.openGlobalSocket()
    });
  }

  openGlobalSocket() {
    this.stompClient.subscribe("/socket-publisher", (message) => {
      this.handleResult(message);
    });
  }

  openSocket() {
    if (this.isLoaded) {
      this.isCustomSocketOpened = true;
      this.stompClient.subscribe("/socket-publisher/"+this.userForm.value.fromId, (message) => {
        this.handleResult(message);
      });
    }
  }

  handleResult(message){
    if (message.body) {
      let messageResult: Messsage = JSON.parse(message.body);
      console.log(messageResult);
      this.messages.push(messageResult);
      
    }
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        idUser: this.userId,
        title: 'Nouveau meeting'
    };

    this.dialog.open(MeetingDialogComponent, dialogConfig);
}
/**public getServerData(event?:PageEvent){//0:0:10:0
  this.meetingService.getMeetingByUser(this.currentUser.id,{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response =>{
      console.log('response'+response)
        this.meetings = response["content"]; 
        //this.datasource = response.data;
        this.pageIndex = event.pageIndex;
        this.pageSize = event.pageSize;
        this.length = response["totalElements"];
      
    },
    error =>{
      // handle error
    }
  );
  return event;
}*/
public getServerDataAllMeeting(event?:PageEvent){//0:0:10:0
  this.meetingService.getMeetingNotConnectByUser(this.userId,{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response =>{
      console.log('response'+response)
        this.meetings = response["content"]; 
        //this.datasource = response.data;
        this.pageIndex = event.pageIndex;
        this.pageSize = event.pageSize;
        this.length = response["totalElements"];
      
    },
    error =>{
      // handle error
    }
  );
  return event;
}
redirectToDelete(id:number){
  console.log('kejkjeekjejekjekjekjekejkejkjj')
  const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        id: id,
        title: 'Supprimer meeting'
    };

    this.dialog.open(MeetingDeleteComponent, dialogConfig);
}
connecteToMeeting(item:Meeting){
  this.meetingService.connectToMeeting(item,this.userId).subscribe(
    response =>{
      console.log('response'+response)
      this.getServerDataAllMeeting(this.pageEvent)
    },
    error =>{
      // handle error
    }
  );
}
redirectToConnect(item:Meeting){
  console.log('kejkjeekjejekjekjekjekejkejkjj')
  const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true; 
      this.meetingService.getConnectByMeeting(item.id,{page:0,size:5,sortBy:this.sortBy,desc:this.desc}).subscribe(data => {
         //this.MessageService.sendMessage("meeting deleted")
         dialogConfig.data = {
          meeting: item,
          users:data,
          title: 'Connecté'
      };
      this.dialog.open(MeetingConnectComponent, dialogConfig);
      })  
}
applyFilter(q:string){
  console.log('str'+q)
  this.meetingService.getMeetingNotConnectByUserSearch(this.userId,{q:q,page:this.pageIndex,size:this.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response => {
      this.meetings = response["content"]; 
      //this.datasource = response.data;
      this.length = response["totalElements"];  });
}
}
