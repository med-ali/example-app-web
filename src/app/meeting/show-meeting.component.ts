import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { User  } from '../_models/user';
import { ITEMS_PER_PAGE } from '../shared/pagination.constant';
import { MatTableDataSource, MatDialogConfig, MatDialog, PageEvent } from '@angular/material';
import { Subscription } from 'rxjs';
import { MessageService  } from '../_services/message.service';
import { Meeting } from '../_models/meeting';
import { ActivatedRoute } from '@angular/router';
import { MeetingService } from '../_services/meetingService';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as Stomp from "@stomp/stompjs"
import * as SockJS from 'sockjs-client';
 import { SocketService } from '../_services/socket.service'; 
import { Messsage } from '../_models/message';
import { AuthenticationService } from '../_services';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-show-meeting-component',
  templateUrl: './show-meeting.component.html',
  styleUrls: ['./show-meeting.component.scss']
})
export class ShowMetingComponentComponent implements OnInit {
  private serverUrl = "http://127.0.0.1:8081/" + 'socket'
  isLoaded: boolean = false; 
  showCode:boolean = false;
  isCustomSocketOpened = false;
  private stompClient;
  private form: FormGroup;
  private userForm: FormGroup;
  messages: Messsage[] = [];
  currentUserSubscription: Subscription;
  currentUser: User;
  isAdmin: boolean;
  message:any;
  userId: string;
  isConnect: boolean;
  pageIndex:number;
  pageSize:number;
  sortBy: string;
  pageEvent: PageEvent;
  meeting: Meeting;
  constructor(private authenticationService: AuthenticationService,private MessageService: MessageService,private userService: UserService,
    private dialog: MatDialog,private route: ActivatedRoute,
    private meetingService: MeetingService,private socketService: SocketService) {
      this.userId =localStorage.getItem('userId');
      this.sortBy = "id"
      //this.loadAllUsers();
      this.pageEvent = new PageEvent();
      this.pageEvent.pageSize = 10000//ITEMS_PER_PAGE;
      this.pageEvent.pageIndex = 0;
      this.getMessageByMeeting(this.pageEvent);
      this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
        this.currentUser = user;   
        
      });
      
      if (localStorage.getItem('isAdmin') == "true") {
        // logged in so return true
        this.isAdmin = true;
      }
   }

  ngOnInit() {
    this.isConnect = false;
    this.getMeeting();
    this.initializeWebSocketConnection();
    //this.openSocket()
    this.form = new FormGroup({
      message: new FormControl(null, [Validators.required])
    })
    this.userForm = new FormGroup({
      fromId: new FormControl(null, [Validators.required]),
      toId: new FormControl(null)
    })
  }
  /*sendMessageUsingSocket() {
    if (this.form.valid) {
      let message: Message = { message: this.form.value.message, fromId: this.currentUser.firstName, toId: this.userForm.value.toId };
      this.stompClient.send("/socket-subscriber/send/message", {}, JSON.stringify(message));
    }
  }*/
  getMessageByMeeting(event?:PageEvent){
    this.meetingService.getMessageByMeeting(this.route["snapshot"]["params"]["id"],{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy}).subscribe(
      response =>{
        console.log('response'+response)
          this.messages = response["content"];
      },
      error =>{
        // handle error
      }
    );
  }
  getMeeting(){
    this.meetingService.getById(this.route["snapshot"]["params"]["id"]).subscribe(
      response =>{
        console.log('response'+response)
          this.meeting = response;
      },
      error =>{
        // handle error
      }
    );
  }
  sendMessageUsingRest() {
    if (this.form.valid) {
    
      let message: Messsage = { message: this.form.value.message,from: this.currentUser , meeting: this.meeting};
      this.socketService.post(message).subscribe(res => {
        console.log(res); 
      })
    }
  }

  initializeWebSocketConnection() {
    let ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.Stomp.over(ws);
    let that = this;
    this.stompClient.connect({}, function (frame) {
      that.isLoaded = true;
      that.openGlobalSocket()
    });
  }

  openGlobalSocket() {
    this.stompClient.subscribe("/socket-publisher", (message) => {
      this.handleResult(message);
    });
  }

  openSocket() {
    console.log("this.currentUser"+this.userId)
    this.isConnect = true;
    if (this.isLoaded) {
      //this.userForm.value.fromId
      this.isCustomSocketOpened = true;
      this.stompClient.subscribe("/socket-publisher/" + this.userId, (message) => {
        this.handleResult(message);
      });
    }
  }

  handleResult(message){
    if (message.body) {
      let messageResult: Messsage = JSON.parse(message.body);
      console.log(messageResult);
      this.messages.push(messageResult);
       
      console.log("new message recieved")
    }
  }
}

