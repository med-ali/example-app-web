import { User } from '.';
import { Meeting } from './meeting';

export interface Messsage {
    message: string,
    from:User,
    meeting: Meeting,
}