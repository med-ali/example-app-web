export class Meeting {
    
    constructor(
        public id: number,
        public code: string,
        public name: string,
        public description: string,
        public createdAt: any, 
        public isDeleted:boolean
      ) {}
}