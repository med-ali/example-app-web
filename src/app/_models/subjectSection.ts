import { Subject } from './subject';
import { Section } from './section';

export class SubjectSection {
    
    constructor(
        public id: number,
        public subject: Subject,
        public section: Section,
        public createdAt: any,
        public updatedAt: string,
        public isDeleted:boolean
      ) {}
}