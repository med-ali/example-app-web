import { Subject } from './subject';
import { User } from './user';
export class Document {
    
    constructor(
        public id: number,
        public name:string,
        public description:string,
        public subject: Subject,
        public user: User,
        public file:any,
        public downloadNumber:number,
        public voteNumber:number,
        public createdAt: any,
        public updatedAt: string,
        public isDeleted:boolean
      ) {}
}