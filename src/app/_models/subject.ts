export class Subject {
    
    constructor(
        public id: number,
        public name: string,
        public description: string,
        public createdAt: any,
        public updatedAt: string,
        public isDeleted:boolean
      ) {}
}