//import { LoginService } from '../login/login.service';
/*import {
  pa 
} from './';*/
import { HttpClientModule } from '@angular/common/http';
//import { FileUploadComponent } from './file-upload/file-upload.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { MessageService } from './service/MessageService';


@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
       
    ],
    providers: [
        
    ],
    entryComponents: [],
    exports: [
      
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class StockSharedModule { }
