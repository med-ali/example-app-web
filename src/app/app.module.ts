import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';
import { AlertService, AuthenticationService,UserService, SectionService, MessageService  } from './_services';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './_guards';
import { AppComponent }  from './app.component';
import { routing }        from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertComponent } from './_directives';
import { MyNavComponent } from './my-nav/my-nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { FormsModule } from '@angular/forms';
//import { MatToolbarModule } from '@angular/material/toolbar'
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule, MatDialogModule, MatMenuModule, MatCardModule, MatTabsModule, MatInputModule, MatPaginatorModule, MatSortModule, MatProgressSpinnerModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatCheckboxModule } from '@angular/material';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { UserComponentComponent } from './user-component/user-component.component';
import { StockSharedModule } from './shared';
import { SectionDialogComponent } from './section/new-section.component';
import { SectionComponent } from './section/section.component';
import { SectionDeleteComponent } from './section/delete-section.component';
import { SubjectComponent } from './subject/subject.component';
import { SubjectService } from './_services/subject.service';
import { SubjectDialogComponent } from './subject/new-subject.component';
import { SubjectDeleteComponent } from './subject/delete-subject.component';
import { SubjectSectionComponent } from './subject/subjectSection.component';
import { SubjectSectionDialogComponent } from './subject/new-subjectsection.component';
import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { SubjectSectionDeleteComponent } from './subject/delete-SubjectSection.component';
import { DocumentComponent } from './document/document.component';
import { DocumentService } from './_services/document.service';
import { DocumentDialogComponent } from './document/new-document.component';
import { NewUserDialogComponent } from './user-component/new-user.component';
import { UserDeleteComponent } from './user-component/delete-user.component';
import { DocumentDeleteComponent } from './document/delete-document.component';
import { MeetingComponent } from './meeting/meeting.component';
import { SocketService } from './_services/socket.service';
import { MeetingService } from './_services/meetingService';
import { MeetingDialogComponent } from './meeting/new-meeting.component';
import { MeetingDeleteComponent } from './meeting/delete-meeting.component';
import { MeetingConnectComponent } from './meeting/connect-meeting.component';
import { MyMeetingComponent } from './meeting/MyMeeting.component';
import { MyConnectMeetingComponent } from './meeting/MyConnectMeeting.component';
import { UserMetingComponentComponent } from './meeting/user-meeting.component';
import { ShowMetingComponentComponent } from './meeting/show-meeting.component';
@NgModule({
  imports: [
      BrowserModule,
      ReactiveFormsModule,
      HttpClientModule,
      routing,
      BrowserAnimationsModule ,
      LayoutModule ,
      MatToolbarModule,
      MatToolbarModule,
      MatButtonModule,
      MatSidenavModule,
      MatIconModule,
      MatListModule,
      MatTableModule,
      MatDialogModule,
      StockSharedModule,
      MatMenuModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatTabsModule,
        MatSidenavModule,
        MatListModule,
        MatToolbarModule,
        MatNativeDateModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        AppRoutingModule,
        MatSelectModule,
        MatDatepickerModule,
        ReactiveFormsModule,
        SelectAutocompleteModule,
        MatCheckboxModule,
        FormsModule
  ],
  declarations: [
      AppComponent,
      AlertComponent,
      HomeComponent,
      LoginComponent,
      RegisterComponent,
      MyNavComponent,
      ChangepasswordComponent,
      UserComponentComponent,
      SectionDialogComponent,
      SectionDeleteComponent,
      SectionComponent,
      SubjectComponent,
      SubjectDialogComponent,
      SubjectDeleteComponent,
      SubjectSectionComponent,
      SubjectSectionDialogComponent,
      SubjectSectionDeleteComponent,
      DocumentComponent,
      DocumentDialogComponent,
      NewUserDialogComponent,
      UserDeleteComponent,
      DocumentDeleteComponent,
      MeetingComponent,
      MeetingDialogComponent,
      MeetingDeleteComponent,
      MeetingConnectComponent,
      MyMeetingComponent,
      MyConnectMeetingComponent,
      UserMetingComponentComponent,
      ShowMetingComponentComponent
     ],
      providers: [
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        MessageService,
        SectionService,
        SubjectService,
        DocumentService,
        SocketService,
        MeetingService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        //fakeBackendProvider
    ],
  bootstrap: [AppComponent],
  entryComponents: [
    SectionDialogComponent,
    SectionDeleteComponent,
    SubjectDialogComponent,
    SubjectDeleteComponent,
    SubjectSectionDialogComponent,
    DocumentDialogComponent,
    NewUserDialogComponent,
    UserDeleteComponent,
    DocumentDeleteComponent,
    MeetingDialogComponent,
    MeetingDeleteComponent,
    MeetingConnectComponent
  ]
})

export class AppModule { }