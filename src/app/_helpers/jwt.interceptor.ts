import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    /*intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const userToken = localStorage.getItem('jwtToken');
        console.log('jwtToken'+userToken)
        const modifiedReq = req.clone({ 
          headers: req.headers.set('Authorization', `Bearer ${userToken}`),
        });
        console.log('AuthorizationjwtToken'+req.headers["Authorization"])
        return next.handle(modifiedReq);
      }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        let jwtToken = localStorage.getItem('jwtToken');
        request.headers.append('Authorization',localStorage.getItem('jwtToken'));
        console.log('jwtToken'+jwtToken)
        if (jwtToken) {
            request.headers.append('Authorization',localStorage.getItem('jwtToken'));
            console.log('jwtTokentre'+jwtToken)
            request.clone({
                setHeaders: { 
                    Authorization: `Bearer ${jwtToken}`
                }
            });
        }

        return next.handle(request);
    }*/
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        request = request.clone({headers: request.headers.set('Authorization', "Bearer " + localStorage.getItem('jwtToken'))});
        request = request.clone({headers: request.headers.set('Accept-Language', 'fr-FR')});
        //request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
        request = request.clone({headers: request.headers.set('Access-Control-Allow-Origin', '*')});
        if (!request.headers.has('Accept')) {
          request = request.clone({headers: request.headers.set('Accept', 'application/json')});
        }
        console.log('Accept-Language:'+request.headers.get("Accept-Language"))
        console.log('Accept-Accept:'+request.headers.get("Accept"))
        console.log('Authorization-Accept:'+request.headers.get("Authorization"))
       // request = request.clone({headers: request.headers.set('Accept-Language', 'fr-FR')});
     
        return next.handle(request);
     
      }
}