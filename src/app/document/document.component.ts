import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService  } from '../_services/message.service';
import { SubjectService, DocumentService, AuthenticationService } from '../_services';
import { MatDialog, MatDialogConfig, MatIconRegistry, PageEvent } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { ITEMS_PER_PAGE } from '../shared';
import { Subject,Document, User } from '../_models';
import { DocumentDialogComponent } from './new-document.component';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { DocumentDeleteComponent } from './delete-document.component'; 

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss']
})
export class DocumentComponent implements OnInit {
  array: number[];
  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  subscription: Subscription;
  message: any;
  section: any;
  documents:Document[];
  subject:Subject;
  docType: string;
  headers: string[];
  currentUserSubscription: Subscription;
  currentUser: User;
  pageEvent: PageEvent;
  pageIndex:number;
  pageSize:number;
  length:number;

  constructor(private domSanitizer: DomSanitizer, private matIconRegistry: MatIconRegistry,private http: HttpClient,
    private authenticationService: AuthenticationService,
    private MessageService: MessageService,private subjectService: SubjectService,private documentService: DocumentService,
    private dialog: MatDialog,private router: Router,private route: ActivatedRoute) {
      this.currentPage = 0;
      this.array = [10,20,30,25,21];
      this.size = ITEMS_PER_PAGE;
      this.sortBy = "id"
      this.desc = -1
      this.subjectService.getById(this.route["snapshot"]["params"]["id"]).subscribe(data=>{
        this.subject = data;
      })
      this.pageEvent = new PageEvent();
      this.pageEvent.pageSize = ITEMS_PER_PAGE;
      this.pageEvent.pageIndex = 0; 
      this.getServerData(this.pageEvent)
      //this.loadAllDocuments()
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "document added now" || message["text"] == "document edited now"|| message["text"] == "document deleted" )  
      {
        //this.loadAllDocuments()
        this.getServerData(this.pageEvent)
        this.message = message;
      }
    });
    this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
      this.currentUser = user;
      console.log("appid" + this.currentUser.id)
    });
    this.matIconRegistry.addSvgIcon(
      'get_app',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/images/get_app-24px.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'add',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/images/add_box-24px.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'like',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/images/favorite_border-24px.svg')
    );
  }
  base64toBlob(byteString) {
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: "octet/stream" });
}
  saveData = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    // a.style = "display: none";
    return function (data, fileName) {
      var json = atob(data),
        blob = this.base64toBlob(json),
        url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = fileName;
      a.click();
      window.URL.revokeObjectURL(url);
    };
}());
  loadAllDocuments() {
    this.documentService.getDocumentsBySubject(this.route["snapshot"]["params"]["id"],{page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
      this.documents = data["content"];;
    });
  }
  public getServerData(event?:PageEvent){//0:0:10:0
    console.log('event'+event.length+":"+event.pageIndex+":"+event.pageSize+":"+event.previousPageIndex)
    this.documentService.getDocumentsBySubject(this.route["snapshot"]["params"]["id"],{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
      response =>{
        console.log('response'+response)
          this.documents = response["content"];
          //this.datasource = response.data;
          this.pageIndex = event.pageIndex;
          this.pageSize = event.pageSize;
          this.length = response["totalElements"];
        
      },
      error =>{
        // handle error
      }
    );
    return event;
  }
  addLike(item:Document){
    this.documentService.addlikeToDocument(this.currentUser.id,item).pipe(first()).subscribe(data => {
      item.voteNumber ++;
      console.log('data'+data)
    });
  }
  delete(item:Document){
    this.documentService.delete(item.id).pipe(first()).subscribe(data => {
    });
  }
  redirectToDelete(id:number){
    const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      dialogConfig.data = {
          id: id,
          title: 'Supprimer document'
      };
  
      this.dialog.open(DocumentDeleteComponent, dialogConfig);
  }
  downloadDocumented(item:Document) {
    item.downloadNumber++ ;
    this.documentService.downloadDocument(item.id).subscribe(res=>{
      //this.docType = value.headers.get('Content-Disposition').replace("attachment; filename=",'');
      console.log("value.headers.get('Content-Disposition')"+res.headers.keys.length)
      const keys = res.headers.keys();
      this.headers = keys.map(key =>
        `${key}: ${res.headers.get(key)}`);
      this.saveData(res.body['res'],res.headers.get('Content-Disposition'));
      console.log("this.headers"+res.headers.get('Content-Disposition'))
    })
  }
  insertData(id:number) {
    this.http.get<any>( `${environment.apiUrl}/api/documents/download/`+id).subscribe((data) => {
        console.log("inserted");
         
    });
}
  ngOnInit() {
  }
  openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
      subject:this.subject,
        id: 1,
        title: 'Nouveau Document'
    };

    this.dialog.open(DocumentDialogComponent, dialogConfig);
}
applyFilter(q:string){
  console.log('str'+q)
  this.documentService.getDocumentsBySubjectWithSearch(this.route["snapshot"]["params"]["id"],{page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc,q:q}).pipe(first()).subscribe(data => { 
    this.documents = data["content"]; 
});
}
}
