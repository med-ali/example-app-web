import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { DocumentService } from '../_services';
@Component({
    selector: 'delete-document',
    templateUrl: './delete-document.component.html'
})
export class DocumentDeleteComponent implements OnInit {
    id: number;
    title:string;
   

    constructor(private MessageService: MessageService,private documentService: DocumentService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<DocumentDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.id = data.id;
            this.title = data.title;
    }

    ngOnInit() {
    }

    delete() {
        this.documentService.delete(this.id).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("document deleted")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}