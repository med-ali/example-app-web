import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { Subject,Document, User } from '../_models';
import { SubjectService, DocumentService, AuthenticationService } from '../_services';
import { Subscription } from 'rxjs';
@Component({
    selector: 'new-document',
    templateUrl: './new-document.component.html',
    styleUrls: ['./new-document.component.scss']
})
export class DocumentDialogComponent implements OnInit {

    form: FormGroup;
    description:string;
    name:string;
    title:string;
    subject:Subject;
    selectedFiles: FileList;  
  currentFileUpload: File;  
    file: any;
    cardImageBase64: any;
    currentUserSubscription: Subscription;
    currentUser: User;
    constructor(private MessageService: MessageService,
        private authenticationService: AuthenticationService,
        private subjectService: SubjectService,
        private documentService: DocumentService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<DocumentDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.subject = data.subject;
        this.name = data.name;
        this.title = data.title;
        this.currentUserSubscription = this.authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
            console.log("appid" + this.currentUser.id)
       });
    }

    ngOnInit() {
        this.form = this.fb.group({
            description: [this.description, []],
            name: [this.name, []],
            file: [this.file, []]
        });
    }
    getBase64(file:File) {
        let me = this;
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (e: any) => {
          //me.modelvalue = reader.result;
          //const imgBase64Path = e.target.result;
       // this.cardImageBase64 = imgBase64Path;
         // console.log("reader.result:----------"+reader.result);
         this.cardImageBase64 = reader.result; 
        };
        reader.onerror = function (error) {
          console.log('Error: ', error);
        };
     }
    save() {
       
        this.getBase64(this.selectedFiles[0])
        const formValue = this.form.value;
        console.log('formValue'+formValue['file'])
        console.log('cardImageBase64'+this.cardImageBase64)
      const newDocument = new Document(
          null,
          formValue['name'],
        formValue['description'],
          this.subject,
          this.currentUser,
          this.cardImageBase64,
          0,
          0,
        null,
        null,
        false
      );
        this.documentService.save(newDocument).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("document added now")
        })
        
        this.dialogRef.close(this.form.value);
    }
    selectFile(event) {  
        const file = event.target.files.item(0);  
       
        
          var size = event.target.files[0].size;  
          if(size > 10000000000000)  
          {  
              alert("size must not exceeds 1 MB");  
              this.form.get('file').setValue("");  
          }  
          else  
          {  
            this.selectedFiles = event.target.files;
            this.getBase64(event.target.files[0])
          }  
         
      
      }     
    close() {
        this.dialogRef.close();
    }
}