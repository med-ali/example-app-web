import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../_models';
import { UserService } from '../_services';
import { ITEMS_PER_PAGE } from '../shared';
import { MatTableDataSource } from '@angular/material';
interface Country {
    name: string;
    flag: string;
    area: number;
    population: number;
  }
  @Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
  })
export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    userss: any[] = [];
   
    COUNTRIES: Country[] = [
        {
          name: 'Russia',
          flag: 'f/f3/Flag_of_Russia.svg',
          area: 17075200,
          population: 146989754
        },
        {
          name: 'Canada',
          flag: 'c/cf/Flag_of_Canada.svg',
          area: 9976140,
          population: 36624199
        },
        {
          name: 'United States',
          flag: 'a/a4/Flag_of_the_United_States.svg',
          area: 9629091,
          population: 324459463
        },
        {
          name: 'China',
          flag: 'f/fa/Flag_of_the_People%27s_Republic_of_China.svg',
          area: 9596960,
          population: 1409517397
        }
      ];
      selectedFiles: FileList;
      currentFileUpload: File;
      progress: { percentage: number } = { percentage: 0 };
      selectedFile = null;
      changeImage = false;
      currentPage: number;
      size: number;
      sortBy: string;
      desc: number;
      public displayedColumns = ['nom', 'prenom',  'email', 'delete'];
      public dataSource = new MatTableDataSource<User>();
    constructor(private userService: UserService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentPage = 0;
        this.size = ITEMS_PER_PAGE;
        this.sortBy = "id"
        this.desc = -1
    }

    ngOnInit() {
        this.loadAllUsers();
        this.userss =[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,]
    }

    deleteUser(id: number) {
        this.userService.delete(id).pipe(first()).subscribe(() => { 
            this.loadAllUsers() 
        });
    }

    private loadAllUsers() {
        this.userService.getAll({page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
            this.users = data["content"];;
            this.dataSource.data = data["content"];
        });
    }
    change($event) {
      this.changeImage = true;
      }
      changedImage(event) {
      this.selectedFile = event.target.files[0];
      }
      upload() {
        this.progress.percentage = 0;
        this.currentFileUpload = this.selectedFiles.item(0);
        console.log('this.currentFileUpload'+this.currentFileUpload.name)
        this.userService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
          console.log('gthis.currentFileUpload'+this.currentFileUpload.name)
        });
      }
      selectFile(event) {
      this.selectedFiles = event.target.files;
      }
}