import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { SubjectService, UserService } from '../_services';
@Component({
    selector: 'delete-user',
    templateUrl: './delete-user.component.html',
    styleUrls: ['./delete-user.component.scss']
})
export class UserDeleteComponent implements OnInit {
    id: number;
    title:string;
   

    constructor(private MessageService: MessageService,private userService: UserService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<UserDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.id = data.id;
            this.title = data.title;
    }

    ngOnInit() {
    }

    delete() {
        this.userService.delete(this.id).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("user deleted now")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}