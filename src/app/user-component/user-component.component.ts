import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { User  } from '../_models/user';
import { ITEMS_PER_PAGE } from '../shared/pagination.constant';
import { MatTableDataSource, MatDialogConfig, MatDialog, PageEvent } from '@angular/material';
import { NewUserDialogComponent } from './new-user.component';
import { Subscription } from 'rxjs';
import { MessageService  } from '../_services/message.service';
import { UserDeleteComponent } from './delete-user.component';

@Component({
  selector: 'app-user-component',
  templateUrl: './user-component.component.html',
  styleUrls: ['./user-component.component.scss']
})
export class UserComponentComponent implements OnInit {
  users: User[];
  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  public displayedColumns = ['nom', 'prenom', 'email', 'update', 'delete'];
  public dataSource = new MatTableDataSource<User>();
  subscription: Subscription; 
  message: any;
  pageEvent: PageEvent;
  datasource: null;
  pageIndex:number;
  pageSize:number;
  length:number;
  constructor(private MessageService: MessageService,private userService: UserService,private dialog: MatDialog) {
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "user added now" || message["text"] == "user edited now"|| message["text"] == "user deleted now" )  
      {
        //this.loadAllUsers()
        this.getServerData(this.pageEvent)
        this.message = message;
      }
    });
   }

  ngOnInit() {
    this.currentPage = 0;
    this.size = ITEMS_PER_PAGE;
    this.sortBy = "id"
    this.desc = -1
    //this.loadAllUsers();
    this.pageEvent = new PageEvent();
    this.pageEvent.pageSize = ITEMS_PER_PAGE;
    this.pageEvent.pageIndex = 0;
    this.getServerData(this.pageEvent)
  }
  
  redirectToDelete(id:number){
    const dialogConfig = new MatDialogConfig();
  
      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;
  
      dialogConfig.data = {
          id: id,
          title: 'Supprimer un utilisateur'
      };
  
      this.dialog.open(UserDeleteComponent, dialogConfig);
  }
  private loadAllUsers() {
    this.userService.getAll({page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
        this.users = data["content"];
        this.dataSource.data = data["content"];
    });
}
openDialog() {
  const dialogConfig = new MatDialogConfig();

  dialogConfig.disableClose = true;
  dialogConfig.autoFocus = true;

  dialogConfig.data = {
      id: 1,
      title: 'Nouveau Utilisateur'
  };
  this.dialog.open(NewUserDialogComponent, dialogConfig);
}
applyFilter(q:string){
  console.log('str'+q)
  this.userService.getAll({q:q,page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
    this.dataSource.data = data["content"];
});
}
public getServerData(event?:PageEvent){//0:0:10:0
  console.log('event'+event.length+":"+event.pageIndex+":"+event.pageSize+":"+event.previousPageIndex)
  this.userService.getAll({page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response =>{
      console.log('response'+response)
        this.users = response["content"];
        this.dataSource.data = response["content"];
        //this.datasource = response.data;
        this.pageIndex = event.pageIndex;
        this.pageSize = event.pageSize;
        this.length = response["totalElements"];
      
    },
    error =>{
      // handle error
    }
  );
  return event;
}

}

