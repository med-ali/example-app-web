import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { SubjectService, AuthenticationService, AlertService, UserService } from '../_services';
import { User } from '../_models';
import { Router } from '@angular/router';
@Component({
    selector: 'new-user',
    templateUrl: './new-user.component.html',
    styleUrls: ['./new-user.component.scss']
})
export class NewUserDialogComponent implements OnInit {

    addForm: FormGroup;
    description:string;
    name:string;
    title:string;
    roles:string[];
    constructor(private userService: UserService,private MessageService: MessageService,private subjectService: SubjectService
        ,private router: Router,
        private fb: FormBuilder,
        private authenticationService: AuthenticationService,
    private alertService: AlertService,
        private dialogRef: MatDialogRef<NewUserDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.name = data.name;
        this.title = data.title;
        this.roles = ["ROLE_ADMIN","ROLE_USER"]
    }
    onOut(){
        console.log("Value after losing focus  is",this.selectedValue);
      }
    selectedValue(arg0: string, selectedValue: any) {
        throw new Error("Method not implemented.");
    }
    ngOnInit() {
        this.addForm = this.fb.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            email: ['', Validators.required],
            password: ['', Validators.required],
            role:['', Validators.required]
          });
    }

    /*save() {
        const formValue = this.addForm.value;
      const user = new User(
          null,
        formValue['name'],
        formValue['description'],
        null,
        null,
        false
      );
        this.subjectService.save(newSubject).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("subject added now")
        })       
        this.dialogRef.close(this.form.value);
    }*/
    onSubmit() {
        const formValue = this.addForm.value;
        const roles = [{id: null , name: formValue['role']}]
        const user = new User(
            null,
            formValue['email'],
            formValue['password'],
            formValue['firstName'],
            formValue['lastName'],
            false,
            roles
        );
        this.userService.register(user)
          .subscribe( data => {
            this.MessageService.sendMessage("user added now")
            this.alertService.success("user added now");
            this.close()
          },
          error =>{
            this.alertService.error(error);
          }    
          );
      }
    close() {
        this.dialogRef.close();
    }
}