import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { SubjectService } from '../_services';
@Component({
    selector: 'delete-subject',
    templateUrl: './delete-subject.component.html',
    styleUrls: ['./delete-subject.component.scss']
})
export class SubjectDeleteComponent implements OnInit {
    id: number;
    title:string;
   

    constructor(private MessageService: MessageService,private subjectService: SubjectService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<SubjectDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.id = data.id;
            this.title = data.title;
    }

    ngOnInit() {
    }

    delete() {
        this.subjectService.delete(this.id).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("subject deleted")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}