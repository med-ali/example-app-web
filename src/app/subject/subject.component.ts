import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatTableDataSource, MatDialog, PageEvent } from '@angular/material';
import { ITEMS_PER_PAGE } from '../shared';
import { Subscription } from 'rxjs';
import { User, Subject } from '../_models';
import { SectionService ,SubjectService} from '../_services';
import { MessageService  } from '../_services/message.service';
import { first } from 'rxjs/operators';
import { SubjectDialogComponent } from './new-subject.component';
import { SubjectDeleteComponent } from './delete-subject.component';
@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {

  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  pageEvent: PageEvent;
  pageIndex:number;
  pageSize:number;
  length:number;
  public displayedColumns = ['name', 'description',  'Document', 'delete'];
  public dataSource = new MatTableDataSource<User>();
  subscription: Subscription;
  message: any;
  subjects:Subject[];
  isAdmin: boolean;
  constructor(private MessageService: MessageService,private subjectService: SubjectService,private sectionService: SectionService,private dialog: MatDialog) { 
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "subject added now" || message["text"] == "subject edited now"|| message["text"] == "subject deleted" )  
      {
        //this.loadAllSubjects()
        this.getServerData(this.pageEvent)
        this.message = message;
      }
    });
    if (localStorage.getItem('isAdmin') == "true") {
      // logged in so return true
      this.isAdmin = true;
    } 
  }

  ngOnInit() {
    this.subjects = [];
    this.currentPage = 0;
    this.size = this.pageSize = ITEMS_PER_PAGE;
    this.sortBy = "id"
    this.desc = -1
    this.pageEvent = new PageEvent();
    this.pageEvent.pageSize = ITEMS_PER_PAGE;
    this.pageEvent.pageIndex = 0; 
    //this.loadAllSections();
    this.getServerData(this.pageEvent)
  }
  private loadAllSubjects() {
    this.subjectService.getAll({page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
        this.dataSource.data = data["content"];
        this.subjects = data["content"];
    });
}
applyFilter(q:string){
  console.log('str'+q)
  this.subjectService.getAllWithSearch({page:this.pageIndex,size:this.size,sortBy:this.sortBy,desc:this.desc,q:q}).pipe(first()).subscribe(data => { 
    this.subjects = data["content"];
});
}
redirectToDelete(id:number){
  const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        id: id,
        title: 'Supprimer subject'
    };

    this.dialog.open(SubjectDeleteComponent, dialogConfig);
}

openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        id: 1,
        title: 'Nouveau subject'
    };

    this.dialog.open(SubjectDialogComponent, dialogConfig);
}
public getServerData(event?:PageEvent){//0:0:10:0
  console.log('event'+event.length+":"+event.pageIndex+":"+event.pageSize+":"+event.previousPageIndex)
  this.subjectService.getAll({page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
    response =>{
      console.log('response'+response)
        this.subjects = response["content"];
        this.dataSource.data = response["content"];
        //this.datasource = response.data;
        this.pageIndex = event.pageIndex;    
        this.pageSize = event.pageSize;
        this.length = response["totalElements"];
      
    },
    error =>{
      // handle error
    }
  );
  return event;
}
}
