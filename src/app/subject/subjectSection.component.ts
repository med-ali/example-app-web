import { Component, OnInit } from '@angular/core';
import { MatDialogConfig, MatTableDataSource, MatDialog, PageEvent } from '@angular/material';
import { ITEMS_PER_PAGE } from '../shared';
import { Subscription } from 'rxjs';
import { User, Subject } from '../_models';
import { SectionService ,SubjectService} from '../_services';
import { MessageService  } from '../_services/message.service';
import { first } from 'rxjs/operators';
import { SubjectDialogComponent } from './new-subject.component';
import { SubjectDeleteComponent } from './delete-subject.component';
import { Router, ActivatedRoute } from '@angular/router';
import { SubjectSectionDialogComponent } from './new-subjectsection.component';
import { SubjectSectionDeleteComponent } from './delete-SubjectSection.component';
@Component({
  selector: 'app-subjectSection',
  templateUrl: './subjectSection.component.html',
  styleUrls: ['./subjectSection.component.scss']
})
export class SubjectSectionComponent implements OnInit {

  currentPage: number;
  size: number;
  sortBy: string;
  desc: number;
  public displayedColumns = ['name', 'description', 'Document','delete'
];
  public dataSource = new MatTableDataSource<User>();
  subscription: Subscription;
  message: any;
  section: any;
  subjects: Subject[];
  pageEvent: PageEvent;
  pageIndex:number;
  pageSize:number;
  length:number;
  constructor(private MessageService: MessageService,private subjectService: SubjectService,
    private sectionService: SectionService,private dialog: MatDialog,private router: Router,private route: ActivatedRoute) { 
      this.sectionService.getById(this.route["snapshot"]["params"]["id"]).pipe(first()).subscribe(data => { 
        this.section = data;
      });
    this.subscription = this.MessageService.getMessage().subscribe(message => { 
      console.log("message"+message["text"])
      if (message["text"] == "subject section added now" || message["text"] == "subject section edited now"|| message["text"] == "subject section deleted" )  
      {
        //this.loadAllSubjects()
        this.getServerData(this.pageEvent)
        this.message = message;
      }
    });  
  }

  ngOnInit() {
    this.currentPage = 0;
    this.size = ITEMS_PER_PAGE;
    this.sortBy = "id"
    this.desc = -1
    this.pageEvent = new PageEvent();
    this.pageEvent.pageSize = ITEMS_PER_PAGE;
    this.pageEvent.pageIndex = 0; 
    this.getServerData(this.pageEvent)
    //this.loadAllSubjects();
  }
  public getServerData(event?:PageEvent){//0:0:10:0
    console.log('event'+event.length+":"+event.pageIndex+":"+event.pageSize+":"+event.previousPageIndex)
    this.subjectService.getSubjectsBySection(this.route["snapshot"]["params"]["id"],{page:event.pageIndex,size:event.pageSize,sortBy:this.sortBy,desc:this.desc}).subscribe(
      response =>{
        console.log('response'+response)
          this.subjects = response["content"];
          //this.datasource = response.data;
          this.pageIndex = event.pageIndex;
          this.pageSize = event.pageSize;
          this.length = response["totalElements"];
        
      },
      error =>{
        // handle error
      }
    );
    return event;
  }
  private loadAllSubjects() {
    this.subjectService.getSubjectsBySection(this.route["snapshot"]["params"]["id"],{page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
        this.dataSource.data = data["content"];
        this.subjects = data["content"];
    });
}
applyFilter(q:string){
  console.log('str'+q)
  this.subjectService.getSubjectsBySectionWithSearch(this.route["snapshot"]["params"]["id"],{q:q,page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).pipe(first()).subscribe(data => { 
    this.dataSource.data = data["content"];
    this.subjects = data["content"];
});
}
redirectToDelete(id:number){
  const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        sectionId: this.route["snapshot"]["params"]["id"],
        subjectId:id,
        title: 'Supprimer matière de section'
    };

    this.dialog.open(SubjectSectionDeleteComponent, dialogConfig);
}

openDialog() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        section: this.section,
        title: 'Nouveau matière à une setcion'
    };

    this.dialog.open(SubjectSectionDialogComponent, dialogConfig);
}

}
