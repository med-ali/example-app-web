import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject, ViewChild } from "@angular/core";
import { FormGroup, FormBuilder, FormControl, NgModel } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { SubjectService } from '../_services';
import { ITEMS_PER_PAGE } from '../shared';
import { Subject } from '../_models';
import { SelectAutocompleteComponent } from 'mat-select-autocomplete';
import { SubjectSection } from '../_models/subjectSection';
@Component({
    selector: 'new-subjectsection',
    templateUrl: './new-subjectsection.component.html',
    styleUrls: ['./new-subjectsection.component.scss']
})
export class SubjectSectionDialogComponent implements OnInit {
    @ViewChild(SelectAutocompleteComponent,null) multiSelect: SelectAutocompleteComponent;
    subjectsForm = new FormControl();
    selectedValue=[];
    form: FormGroup;
    description:string;
    name:string;
    title:string;
    sectionId:number;
    currentPage: number;
    size: any;
    sortBy: string;
    desc: number;
    subjects: Subject[];
    subjectSection: SubjectSection[];
    section: any;
    constructor(private MessageService: MessageService,private sectionService: SectionService,
        private subjectService: SubjectService,
        private dialogRef: MatDialogRef<SubjectSectionDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.name = data.name;
        this.title = data.title;
        this.section = data.section;
        this.sectionId = this.section.id;
    }
    onOut(){
        console.log("Value after losing focus  is",this.selectedValue);
      }
    ngOnInit() {
        this.subjectSection = [];
        this.currentPage = 0;
        this.size = ITEMS_PER_PAGE;
        this.sortBy = "id"
        this.desc = -1
        this.subjectService.getSubjectsBySectionNotAssociated(this.sectionId,{page:this.currentPage,size:this.size,sortBy:this.sortBy,desc:this.desc}).subscribe(data=>{
            console.log('data'+data)
            this.subjects = data["content"]
        })
        
    }
    selectAll(checkAll, select: NgModel, values) {
        //this.toCheck = !this.toCheck;
        if(checkAll){
          select.update.emit(values); 
        }
        else{
          select.update.emit([]);
        }
      }
    save() {
     console.log("selected")
     this.onOut()
     this.selectedValue.forEach(s=>{
         const newSubjectSection = new SubjectSection (
             null,
             s,
             this.section,
             null,
             null,
             false
            )
            this.subjectSection.push(newSubjectSection)
     })
     this.sectionService.saveSectionSubject(this.section.id,this.subjectSection).subscribe(data=>{
         console.log('fafafafafa'+data)
         this.close()
         this.MessageService.sendMessage("subject section added now")
     })
     
    }
    
    close() {
        this.dialogRef.close();
    }
}