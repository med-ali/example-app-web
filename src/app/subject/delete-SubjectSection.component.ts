import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";
import { Component, OnInit, Inject } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { SectionService } from '../_services/section.service';
import { Section } from '../_models/section';
import { MessageService  } from '../_services/message.service';
import { SubjectService } from '../_services';
@Component({
    selector: 'delete-subjectSection',
    templateUrl: './delete-SubjectSection.component.html',
    styleUrls: ['./delete-SubjectSection.component.scss']
})
export class SubjectSectionDeleteComponent implements OnInit {
    subjectId: number;
    sectionId: number;
    title:string;
   

    constructor(private MessageService: MessageService,private subjectService: SubjectService,
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<SubjectSectionDeleteComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
            this.subjectId = data.subjectId;
            this.sectionId = data.sectionId;
            this.title = data.title;
    }

    ngOnInit() {
    }

    delete() {
        this.subjectService.deleteSubjectSection(this.subjectId,this.sectionId).subscribe(data => {
           console.log("data")
           console.log(data)
           this.MessageService.sendMessage("subject section deleted")
        })
        
        this.dialogRef.close();
    }

    close() {
        this.dialogRef.close();
    }
}