import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService, AuthenticationService } from '../_services';
import { error } from '@angular/compiler/src/util';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router, private authenticationService: AuthenticationService,
    private alertService: AlertService) { }

  addForm: FormGroup;

  ngOnInit() {
    this.addForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
    });

  }

  onSubmit() {
    this.authenticationService.createUser(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['login']);    
      },
      error =>{
        this.alertService.error(error);
      }    
      );
  }

}
